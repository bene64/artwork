#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2016 Pander <pander@users.sourceforge.net>
# SPDX-FileCopyrightText: 2022 Hans-Christoph Steiner <hans@eds.org>
# SPDX-FileCopyrightText: 2024 linsui <linsui@inbox.lv>
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from pathlib import Path
from xml.sax.saxutils import escape

import yaml

rtl = ("ar", "arc", "dv", "fa", "ha", "he", "khw", "ks", "ku", "ps", "ur", "yi")
entities = {"'": "&apos;"}
shrink = ";word-spacing:-5px;letter-spacing:-1px"

fonts = {
    "ar": "Noto Sans Arabic",
    "ja": "Noto Sans CJK JP",
    "ko": "Noto Sans CJK KR",
    "zh-cn": "Noto Sans CJK SC",
    "zh-hk": "Noto Sans CJK TC",
    "zh-tw": "Noto Sans CJK TC",
}

d = Path(__file__).parent
os.chdir(d)
Path("src/en").mkdir(exist_ok=True)
Path("src/en/strings.yml").write_text(Path("src/strings.yml").read_text())
for f in d.glob("src/*/strings.yml"):
    locale = f.parent.name
    with open(f) as fp:
        strings = yaml.safe_load(fp)
    top = strings.get("above F-Droid", "").strip()
    bottom = strings.get("below F-Droid", "").strip()
    if top and bottom:
        name = "top_and_bottom"
    elif top:
        name = "top"
    elif bottom:
        name = "bottom"

    if locale in rtl:
        direction = "rtl"
    else:
        direction = "ltr"

    with open("src/%s_%s.svg" % (name, direction)) as fp:
        source = fp.read().strip()

    if not os.path.isdir("src/%s" % locale):
        os.mkdir("src/%s" % locale)
    with open(os.path.join("src", locale, "strings.yml"), "w") as fp:
        yaml.dump(
            {"above F-Droid": top.lower(), "below F-Droid": bottom.lower()},
            fp,
            default_flow_style=False,
            allow_unicode=True,
        )

    output = source
    top = escape(top.upper(), entities=entities)
    bottom = escape(bottom.upper(), entities=entities)
    if len(top) > 15:
        output = output.replace('">TOP', shrink + '">TOP')
    output = output.replace("TOP", top).replace("BOTTOM", bottom)
    if fonts.get(locale, None):
        output = output.replace("DejaVu Sans", fonts[locale])
    with open("get-it-on-%s.svg" % locale, "w") as fp:
        fp.write(output)
