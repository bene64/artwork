#!/usr/bin/env sh

# SPDX-FileCopyrightText: 2016 Pander <pander@users.sourceforge.net>
# SPDX-License-Identifier: GPL-3.0-or-later

if [ $(flatpak list|grep org.inkscape.Inkscape|wc -l) -gt 0 ]; then
    inkscape=$(echo flatpak run org.inkscape.Inkscape)
else
    inkscape=$(echo inkscape)
fi

for i in *.svg; do
    svgo -i $i
    $inkscape $i -To $i 2>&1 >/dev/null
done
